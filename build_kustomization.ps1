kubectl create namespace biletado
kubectl config set-context --current --namespace biletado
kubectl apply -k . -l app.kubernetes.io/part-of=biletado -n biletado
kubectl wait pods -n biletado -l app.kubernetes.io/part-of=biletado --for condition=Ready --timeout=120s