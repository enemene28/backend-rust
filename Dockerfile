FROM rust:1.75 as builder
WORKDIR /usr/src/backend-rust
COPY . .
RUN cargo install --path .

FROM debian:bookworm-slim
EXPOSE 9000
RUN apt-get update && apt-get install -y postgresql-client && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/backend-rust /usr/local/bin/backend-rust
ENV POSTGRES_ASSETS_USER=postgres
ENV POSTGRES_ASSETS_PASSWORD=postgres
ENV POSTGRES_ASSETS_DBNAME=assets
ENV POSTGRES_ASSETS_HOST=localhost
ENV POSTGRES_ASSETS_PORT=5432
ENV MY_LOG_LEVEL=info
ENV MY_LOG_STYLE=auto
CMD ["backend-rust"]