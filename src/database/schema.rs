use diesel::table;

table! {
    buildings (id) {
        id -> diesel::sql_types::Uuid,
        name -> diesel::sql_types::Text,
        address -> Nullable<diesel::sql_types::Text>,
        deleted_at -> Nullable<diesel::sql_types::Timestamptz>,
    }
}

table! {
    storeys (id) {
        id -> diesel::sql_types::Uuid,
        name -> diesel::sql_types::Text,
        building_id -> diesel::sql_types::Uuid,
        deleted_at -> diesel::sql_types::Timestamptz,
    }
}

table! {
    rooms (id) {
        id -> diesel::sql_types::Uuid,
        name -> diesel::sql_types::Text,
        storey_id -> diesel::sql_types::Uuid,
        deleted_at -> diesel::sql_types::Timestamptz,
    }
}