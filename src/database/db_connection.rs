use diesel::pg::PgConnection;
use diesel::prelude::*;
use dotenvy::dotenv;
use std::env;

pub fn establish_connection() -> Result<PgConnection, ConnectionError> {
    dotenv().ok();
    let database_user = env::var("POSTGRES_ASSETS_USER").expect("POSTGRES_ASSETS_USER must be set");
    let database_pw = env::var("POSTGRES_ASSETS_PASSWORD").expect("POSTGRES_ASSETS_USER must be set");
    let database_name = env::var("POSTGRES_ASSETS_DBNAME").expect("POSTGRES_ASSETS_DBNAME must be set");
    let database_host = env::var("POSTGRES_ASSETS_HOST").expect("POSTGRES_ASSETS_HOST must be set");
    let database_port = env::var("POSTGRES_ASSETS_PORT").expect("POSTGRES_ASSETS_PORT must be set");
    let database_url = format!("postgres://{}:{}@{}:{}/{}", database_user, database_pw, database_host, database_port, database_name);
    println!("{}", database_url);
    PgConnection::establish(&database_url)
//        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}

pub fn check_connection() -> bool {
    let result: bool;
    match establish_connection() {
        Ok(_connection) => result = true,
        Err(_error) => result = false
    };
    result
}