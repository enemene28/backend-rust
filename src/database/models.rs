use diesel::prelude::*;
use uuid::Uuid;
use chrono::NaiveDateTime;
use serde::{Serialize, Deserialize};


#[derive(Queryable, Selectable, Identifiable, Serialize, Deserialize, Insertable)]
#[diesel(table_name = crate::database::schema::buildings)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Building {
    pub id: Uuid,
    pub name: String,
    pub address: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub deleted_at: Option<NaiveDateTime>
}

#[derive(Queryable, Selectable, Identifiable)]
#[diesel(table_name = crate::database::schema::storeys)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Storey {
    pub id: Uuid,
    pub name: String,
    pub building_id: Uuid,
    pub deleted_at: NaiveDateTime
}

#[derive(Queryable, Selectable)]
#[diesel(table_name = crate::database::schema::rooms)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Room {
    pub id: Uuid,
    pub name: String,
    pub storey_id: Uuid,
    pub deleted_at: NaiveDateTime
}

// id         uuid default uuid_generate_v4() not null
// primary key,
// "name"     text                            not null,
// address    text,
// deleted_at timestamptz


// create table if not exists storeys
// (
//     id          uuid default uuid_generate_v4() not null
//         primary key,
//     "name"      text                            not null,
//     building_id uuid                            not null
//         references buildings,
//     deleted_at  timestamptz
// );

// alter table storeys
//     owner to postgres;

// create table if not exists rooms
// (
//     id         uuid default uuid_generate_v4() not null
//         primary key,
//     "name"     text                            not null,
//     storey_id  uuid                            not null
//         references storeys,
//     deleted_at timestamptz
// );
