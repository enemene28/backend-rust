use diesel::{QueryDsl, RunQueryDsl};
use log::info;
use uuid::Uuid;

use crate::database::schema::buildings::dsl::buildings;

use super::{db_connection::establish_connection, models::Building};



pub fn get_buildings_from_db() -> Vec<Building> {
    let connection = &mut establish_connection().unwrap();
    let buildings_all = buildings.load::<Building>(connection).expect("Error loading buildings...");
    info!("Fetched these buildings: {}", buildings_all.len());
    buildings_all
}

pub fn get_building_from_db_by_id(identifier: uuid::Uuid) -> Building {
    let connection = &mut establish_connection().unwrap();
    let building = buildings.find(identifier).first::<Building>(connection).expect("Error loading buildings...");
    info!("Fetched building...");
    building
}

pub fn post_building_to_db(name: String, address: String) -> Result<Building, diesel::result::Error> {
    let connection = &mut establish_connection().unwrap();
    let new_building = Building {
        id: Uuid::new_v4(),
        name: name,
        address: Some(address),
        deleted_at: None
    };
    //TODO check if building with same name and address already exists???
    diesel::insert_into(buildings)
        .values(new_building)
        .get_result::<Building>(connection)
}

pub fn put_building_to_db(id: Uuid, name: String, address: String, deleted_at: Option<String>) -> Result<Building, diesel::result::Error> {
    let connection = &mut establish_connection().unwrap();
    let result = buildings.find(id).first::<Building>(connection);
    let new_building;
    if result.is_err() {
        new_building = Building {
            id: id,
            name: name,
            address: Some(address),
            deleted_at: None
        };
        
    
    } else {
        if result.unwrap().deleted_at.is_none() {
            return Err(diesel::result::Error::NotFound)
        } else {
            if deleted_at.unwrap() == "null" {
                new_building = Building {
                    id: id,
                    name: name,
                    address: Some(address),
                    deleted_at: None
                };
            } else {
                return Err(diesel::result::Error::NotFound)
            }
        }
        
        
    }
    //TODO check if building with same name and address already exists???
    diesel::insert_into(buildings)
        .values(new_building)
        .get_result::<Building>(connection)
}