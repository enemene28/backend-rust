use actix_web::{get, HttpResponse, Responder};
use serde_json::json;
use crate::database::db_connection::check_connection;

#[get("/api/v2/assets/status")]
async fn get_status() -> impl Responder {
    let status = json!({
        "authors": [
          "Michael Enzmann"
        ],
        "api_version": "2.1.0"
      });
    HttpResponse::Ok().json(status)
}

#[get("/api/v2/assets/health")]
async fn get_health() -> impl Responder {
    let liveness = true;
    let readyness = check_connection();
    let response;
    if (liveness == true) || (readyness == true) {
        let health = json!({
            "live": liveness,
            "ready": readyness,
            "databases": {
              "assets": {
                "connected": readyness
              }
            }
          });
        response = HttpResponse::Ok().json(health);
    } else {
        let bad_request = json!({
            "errors": [
              {
                "code": "bad_request",
                "message": "string",
                "more_info": "string"
              }
            ],
            "trace": "18d3cf98-1710-4000-801b-3d026d09c201"
          });
        response = HttpResponse::ServiceUnavailable().json(bad_request);
    }
    response
}

#[get("/api/v2/assets/health/live")]
async fn get_health_live() -> impl Responder {
    let liveness = true;
    let response;
    if liveness == true {
        let health_live = json!({
            "live": liveness
          });
        response = HttpResponse::Ok().json(health_live)
    } else {
        let bad_request = json!({
            "errors": [
              {
                "code": "bad_request",
                "message": "string",
                "more_info": "string"
              }
            ],
            "trace": "18d3cf26-3930-4000-8803-ab59c1cd8480"
          });
          response = HttpResponse::ServiceUnavailable().json(bad_request);
    }
    response
}

#[get("/api/v2/assets/health/ready")]
async fn get_health_ready() -> impl Responder {
    let readyness = check_connection();
    let response;
    if readyness == true {
        let health_ready = json!({
            "ready": readyness
          });
        response = HttpResponse::Ok().json(health_ready)
    } else {
        let bad_request = json!({
            "errors": [
              {
                "code": "bad_request",
                "message": "string",
                "more_info": "string"
              }
            ],
            "trace": "18d382ad-6b20-4000-87f9-c5d0ed7e2301"
          });
        response = HttpResponse::ServiceUnavailable().json(bad_request)
    }
    response
}

