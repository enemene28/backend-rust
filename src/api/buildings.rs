use std::str::FromStr;

use actix_web::{delete, get, http, post, put, web, HttpResponse, Responder};
use log::{debug, info, error};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::database::db_buildings::{get_building_from_db_by_id, get_buildings_from_db, post_building_to_db, put_building_to_db};

#[get("/api/v2/assets/buildings")]
async fn get_buildings(body: Option<web::Query<QueryDeleted>>) -> impl Responder {
    info!("GET /api/v2/assets/buildings");

    let default = web::Query::<QueryDeleted>::from_query("include_deleted=false").unwrap();
    let body_query = body.unwrap_or_else(|| default);

    let include_deleted = &body_query.include_deleted;

    debug!("Include deleted buildings?: {}", include_deleted);

    let mut buildings = get_buildings_from_db();
    if include_deleted.eq_ignore_ascii_case("true") {
        HttpResponse::Ok().json(buildings)
    } else {
        buildings.retain(|building| building.deleted_at == None);
        HttpResponse::Ok().json(buildings)
    }
}

#[get("/api/v2/assets/buildings/{id}")]
async fn get_buildings_by_id(id_path: web::Path<String>) -> impl Responder {
    let id_uuid = Uuid::parse_str(&id_path.to_string()).expect("Error parsing uuid...");
    let building = get_building_from_db_by_id(id_uuid);
    //TODO error response
    HttpResponse::Ok().json(building)
}

#[delete("/api/v2/assets/buildings/{id}")]
async fn delete_buildings() -> HttpResponse {
    HttpResponse::Ok().finish()
}

#[put("/api/v2/assets/buildings/{id}")]
async fn put_buildings(id_path: web::Path<String>, payload: web::Json<NewBuilding>) -> impl Responder {
    let id_uuid = Uuid::parse_str(&id_path.to_string()).expect("Error parsing uuid...");
    let name = payload.name.clone();
    let address = payload.address.clone();
    let deleted_at = payload.deleted_at.clone().unwrap(); //TODO Error case --> don't panic
    debug!("name: {}", name);
    debug!("address: {}", address);
    debug!("uuid: {}", id_uuid);
    debug!("deleted_at: {}", deleted_at);
    Some(deleted_at);
    let new_building = put_building_to_db(id_uuid, name, address, deleted_at).unwrap();
    //FIXME implement put
    HttpResponse::Ok().json(new_building)
}

#[post("/api/v2/assets/buildings")]
async fn post_buildings(payload: web::Json<NewBuilding>) -> HttpResponse {
    let name = payload.name.clone();
    let address = payload.address.clone();
    debug!("name: {}", name);
    debug!("address: {}", address);
    if !is_printable_one_space(&name) {
        let api_error = BiletadoApiError { code: http::StatusCode::BAD_REQUEST.as_u16(), message: "Bad request".to_string(), more_info: "Bad request".to_string()};
        let error_list = vec![api_error];
        let bad_request = BiletadoApiErrorResponse { errors: error_list ,
                                                                                trace: uuid::Uuid::from_str("18d6baa3-2420-4000-8f39-9fb3fdcd3380").unwrap()};
        return HttpResponse::BadRequest().json(bad_request)
    }
    let new_building = post_building_to_db(name, address).unwrap();

    HttpResponse::Ok().json(new_building)
}

#[derive(Deserialize)]
pub struct QueryDeleted {
    include_deleted: String,
}

impl Default for QueryDeleted {
    fn default() -> Self {
        QueryDeleted {
            include_deleted: "false".to_string(),
        }
    }
}

#[derive(Deserialize)]
pub struct NewBuilding {
    name: String,
    address: String,
    deleted_at: Option<String>
}

pub fn is_printable_one_space(word: &String) -> bool {
    //TODO Test for non-printable characters
    let mut space_number: u8 = 0;
    let space = ' ';
    for letter in word.chars() {
        if !letter.is_ascii_alphanumeric() {
            if letter.eq_ignore_ascii_case(&space) {
                space_number = space_number + 1;
                if space_number > 1 {
                    error!("Too many spaces in a row: {}. Only one space allowed!", space_number);
                    return false
                }
            } 
        } else {
            space_number = 0;
        }
    }
    true
}

#[derive(Serialize, Deserialize)]
pub struct BiletadoApiErrorResponse {
    errors: Vec<BiletadoApiError>,
    trace: uuid::Uuid
}

#[derive(Serialize, Deserialize)]
pub struct BiletadoApiError {
    code: u16,
    message: String,
    more_info: String,
}