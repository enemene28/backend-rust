use actix_web::{get, delete, put, post, HttpResponse, Responder};
use serde_json::json;


#[get("/api/v2/assets/rooms")]
async fn get_rooms() -> impl Responder {
    let health_ready = json!({
        "rooms": [
          {
            "id": "44b0a13f-ca5a-4ce1-bd93-bfe874e92bb7",
            "name": "200",
            "storey_id": "f6536bdf-037e-4ee4-aede-a44358e827c4"
          },
          {
            "name": "404",
            "id": "c3deae20-baaa-4f2c-931c-fbfea5a6fe6a",
            "storey_id": "d41b784c-86b2-4283-8a2f-e1eab1d980d0",
            "deleted_at": "2023-11-06T05:28:59.000Z"
          }
        ]
      });
    HttpResponse::Ok().json(health_ready)
}

#[get("/api/v2/assets/rooms/{id}")]
async fn get_rooms_by_id() -> impl Responder {
    let health_ready = json!({
        "id": "44b0a13f-ca5a-4ce1-bd93-bfe874e92bb7",
        "name": "200",
        "storey_id": "f6536bdf-037e-4ee4-aede-a44358e827c4"
      });
    HttpResponse::Ok().json(health_ready)
}

#[delete("/api/v2/assets/rooms/{id}")]
async fn delete_rooms_by_id() -> HttpResponse {
    HttpResponse::Ok().finish()
}

#[put("/api/v2/assets/rooms/{id}")]
async fn put_rooms_by_id() -> HttpResponse {
    HttpResponse::Ok().finish()
}

#[post("/api/v2/assets/rooms")]
async fn post_rooms() -> HttpResponse {
    HttpResponse::Ok().finish()
}