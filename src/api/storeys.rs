use actix_web::{get, put, post, delete, HttpResponse, Responder};
use serde_json::json;


#[get("/api/v2/assets/storeys")]
async fn get_storeys() -> impl Responder {
    let health_ready = json!({
        "storeys": [
          {
            "id": "f6536bdf-037e-4ee4-aede-a44358e827c4",
            "name": "subtegmento",
            "building_id": "5fcaa865-4148-4553-9ee5-5666fceed56a"
          },
          {
            "name": "kelo",
            "id": "d41b784c-86b2-4283-8a2f-e1eab1d980d0",
            "building_id": "cf731505-f4f6-412f-992f-1cf24874bac3",
            "deleted_at": "2023-11-06T05:29:59.000Z"
          }
        ]
      });
    HttpResponse::Ok().json(health_ready)
}

#[get("/api/v2/assets/storeys/{id}")]
async fn get_storey_by_id() -> impl Responder {
    let health_ready = json!({
        "id": "f6536bdf-037e-4ee4-aede-a44358e827c4",
        "name": "subtegmento",
        "building_id": "5fcaa865-4148-4553-9ee5-5666fceed56a"
      });
    HttpResponse::Ok().json(health_ready)
}

#[delete("/api/v2/assets/storeys/{id}")]
async fn delete_storey() -> HttpResponse {
    HttpResponse::Ok().finish()
}

#[put("/api/v2/assets/storeys/{id}")]
async fn put_storey_by_id() -> HttpResponse {
    HttpResponse::Ok().finish()
}

#[post("/api/v2/assets/storeys")]
async fn post_storey() -> HttpResponse {
    HttpResponse::Ok().finish()
}