use actix_cors::Cors;
use actix_web::{App, HttpServer};
use dotenvy::dotenv;
use log::{debug, error, info, trace, warn};
use env_logger::Env;

mod api;
mod database;

use crate::api::status::*;
use crate::api::buildings::*;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    let env = Env::default().filter_or("MY_LOG_LEVEL", "trace").write_style_or("MY_LOG_STYLE", "always");
    env_logger::init_from_env(env);
    
    debug!("Testing debug..");
    error!("Testing error..");
    trace!("Testing trace..");
    warn!("Testing debug..");
    info!("Starting API Http server....");
    HttpServer::new(|| {
        let cors = Cors::permissive();
        App::new()
            .wrap(cors)
            .service(get_status)
            .service(get_health)
            .service(get_health_live)
            .service(get_health_ready)
            .service(get_buildings)
            .service(get_buildings_by_id)
            .service(post_buildings)
    })
        .bind(("0.0.0.0", 9000))?
        .run()
        .await
}