podman run -d `
    --name="gitlab-runner" `
    -v /var/run/podman:/var/run/podman `
    -v gitlab-runner-config:/etc/gitlab-runner `
    registry.gitlab.com/gitlab-org/gitlab-runner:latest


podman run --rm -v gitlab-runner-config:/etc/gitlab-runner registry.gitlab.com/gitlab-org/gitlab-runner:latest `
    register `
  --non-interactive `
  --url "https://gitlab.com/" `
  --token "$RUNNER_TOKEN" `
  --executor "docker" `
  --docker-host "unix:///var/run/podman/podman.sock" `
  --docker-privileged `
  --docker-image alpine:latest `
  --description "podman-THINKPAD-ME-ulixe"