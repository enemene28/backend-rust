# backend-rust

This is a implementation of the assets api backend of the biletado project.  
The service is implemented in the Rust programming language, using the [actix-web](https://actix.rs/) framework and [diesel](http://diesel.rs/) ORM.

## Building the project locally

### Requirements

- Rust 1.75 (with all of the used crates)
    - actix-web as http-server
    - diesel as ORM
- PostgreSQL
    - libpq has to be installed
    - subfolders *bin* and *lib* of PostgreSQL installation have to be added to path environment variables (Windows)

Rust projects can be built with the `cargo`-tool of the Rust Toolchain.  

examples:
`cargo build` for a local build of the project
`cargo run` to run the project

## Building the project online

Windows: Run the build_kustomization.ps1 script.

## Environment Variables

| Name                     | Description                         |
| ------------------------ | ----------------------------------- |
| POSTGRES_ASSETS_USER     | Name of Postgres database user      |
| POSTGRES_ASSETS_PASSWORD | Postgres database user password     |
| POSTGRES_ASSETS_DBNAME   | Name of Postres assets database     |
| POSTGRES_ASSETS_HOST     | IP of Postgres assets database Host |
| POSTGRES_ASSETS_PORT     | Number of Postges assets port       |
| MY_LOG_LEVEL             | Log Level of Logging feature        |
| MY_LOG_STYLE             | Color style of logging messages     |

## Logging

Available Log Levels (in descending severity):  

- error
- warn
- info
- debug  
- trace  
- off (pseudo log level that turns all logging off)

The log level can be set with the `MY_LOG_LEVEL` environment variable.

There are also different color styles for logging messages available:

- always
- auto  
- never  

These color styles can be set with the `MY_LOG_STYLE` environment variable.
