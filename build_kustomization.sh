!#/usr/bin/bash
# create the biletado namespace
kubectl create namespace biletado
# optional: set the curren default namespace to "biletado" for easier work with kubectl without "-n"
kubectl config set-context --current --namespace biletado
# apply the application into the new namespace and prune outdated objects
kubectl apply -k . --prune -l app.kubernetes.io/part-of=biletado -n biletado # ignore deprecation warning
# wait for all pods to be available
kubectl wait pods -n biletado -l app.kubernetes.io/part-of=biletado --for condition=Ready --timeout=120s